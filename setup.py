from setuptools import setup


setup(
    name = "btree_lib",
    version = "1.1",
    author = "Gleb",
    author_email = "gleb@glebmail.xyz",
    description = ("The lib for create btree trees"),
    license = "GPL 3",
    url = "https://git.glebmail.xyz/PythonPrograms/btree_lib",
    packages=['btree_lib'],
)